//
//  AppDelegate.h
//  testerZ0rZ
//
//  Created by Peter Molignano on 8/20/14.
//  Copyright (c) 2014 land of gords. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
